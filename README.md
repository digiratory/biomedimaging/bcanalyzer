# BCAnalyzer: A semi-automated tool for scratch assay and others biomedical research and applications

![PyPI - Downloads](https://img.shields.io/pypi/dm/bio-cntrs-analyzer?style=plastic)
![PyPI - Python Version](https://img.shields.io/pypi/pyversions/bio-cntrs-analyzer)
![PyPI - Wheel](https://img.shields.io/pypi/wheel/bio-cntrs-analyzer)
![PyPI](https://img.shields.io/pypi/v/bio-cntrs-analyzer)
![PyPI - License](https://img.shields.io/pypi/l/bio-cntrs-analyzer)

[![DOI:10.1016/j.bspc.2022.104189](https://zenodo.org/badge/DOI/10.1016/j.bspc.2022.104189.svg)](https://doi.org/10.1016/j.bspc.2022.104189)
[![Citation Badge](https://api.juleskreuer.eu/citation-badge.php?doi=10.1016/j.bspc.2022.104189)](https://juleskreuer.eu/projekte/citation-badge/)

Segmentation of patchy areas in biomedical images based on local edge density estimation

## Installation

### Download executable from release page
 
We provide a builded windows executable file for every release. You can find the latest whl or exe file on the [release page](https://gitlab.com/digiratory/biomedimaging/bcanalyzer/-/releases). Also, you can find all versions of windows execution files in the [archive](https://drive.digiratory.ru/d/s/nCQF5QKnlOoLyvoogzIMazY5YAOZDDmy/hY8ac89PJtnKtduC6w9CEkRtLVVRmZ4_-qbCgo_pvPwk).


### Installation from binaries

```bash
pip install -U bio-cntrs-analyzer
```
### Build exe from source

From venv:

```bash
pip install cx_Freeze
python setup.py build
```

## User manual

For starting appliation evualate in command line (terminal) the next command:

```bash
bcanalyzer
```

![UserManualFigure](https://gitlab.com/digiratory/biomedimaging/bcanalyzer/-/raw/main/images/UserManualFigure.PNG) 

Software user interface outline: (A) list of images submitted for the analysis; (B) segmentation algorithm options, including (C) color channel import options; (D) selected image with on-the-fly visualization of the segmentation results; (E) segmentation algorithm controls for the online adjustment of its sensitivity and resolution; (F) file export menu.

The typical algorithm of user interaction with the software is as follows:

* Selected images are imported by their drag-and-drop onto the program window. The image list appears in **A**.
* Global algorithm options can be adjusted in **B** and color channels for the analysis selected in **C**.  
* The image selected in the list **A** is displayed in the panel **D** with immediate visualization of the segmentation results (using default parameters during the first run).
* Next the algorithm parameters (sensitivity and resolution) can be adjusted manually in **E**, given that the automated threshold option is disabled, although one can also apply the automated threshold selection for the first approach and then disable it in order to proceed with manual fine tuning. Segmentation results are visualized on-the-fly for direct user control. Of note, global options and color channel selection can be readjusted at this stage as well. Following necessary adjustments, the chosen algorithm parameters can be applied either to the entire imported image set, or solely to the currently analyzed image, with corresponding controls available in **E**.
* Once the algorithm parameters are adjusted either for a single or for a few representative image(s), further processing and export can be performed as a fully automated procedure for the entire image set using file export options in **F**. Export options include visualizations of segmentation results, either as binary masks or as marked-up images similar to those appearing on the screen during the analysis, or both of them, as well as a *.csv table with summary statistics.

# Dataset

Link to downloading dataset:

https://drive.digiratory.ru/d/s/mrbRyk4HyFbOIEANc6DhQGxhCFNgq3xI/Jqbcbsq_eE5Cf8-bnJaM3dXL6RI1v7d7-d74AOY4RLgk

If something goes wrong, please, write to amsinitca[at]etu.ru

# Troubleshooting

## qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.

### Problem

```bash
qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.
This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.
Available platform plugins are: eglfs, linuxfb, minimal, minimalegl, offscreen, vnc, wayland-egl, wayland, wayland-xcomposite-egl, wayland-xcomposite-glx, webgl, xcb.
Aborted (core dumped)
```

### Solution

```bash
sudo apt-get install libxcb-xinerama0
```

# Citation

If you find this project useful, please cite:

```bib
@article{SINITCA2023104189,
    title = {Segmentation of patchy areas in biomedical images based on local edge density estimation},
    journal = {Biomedical Signal Processing and Control},
    volume = {79},
    pages = {104189},
    year = {2023},
    issn = {1746-8094},
    doi = {https://doi.org/10.1016/j.bspc.2022.104189},
    url = {https://www.sciencedirect.com/science/article/pii/S1746809422006437},
    author = {Aleksandr M. Sinitca and Airat R. Kayumov and Pavel V. Zelenikhin and Andrey G. Porfiriev and Dmitrii I. Kaplun and Mikhail I. Bogachev},
}
```

