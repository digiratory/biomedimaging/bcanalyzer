from cx_Freeze import setup, Executable

executables = [Executable('bcanalyzer/app.py',
                          target_name='bcanalyzer.exe',
                          base='Win32GUI',
                          icon='./bcanalyzer/gui/resources/logo.ico')]

options = {"build_exe":
           {
               "packages": [],
               # 'zip_include_packages': ['*'],
               'includes': ['PyQt5'],
               "excludes": ["tkinter"],
               'build_exe': 'Bio-Cntrs-Analyzer-WinExe'
           }
           }


setup(name='Bio-Cntrs-Analyzer',
      version='0.0.9',
      description='The semi-automatic segmentation and quantification of patchy areas in various biomedical images based on the assessment of their local edge densities.',
      executables=executables,
      options=options)
