import os
import sys


if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.dirname(__file__)))
    import bcanalyzer.app as bcanalyzer_app
    bcanalyzer_app.main()
